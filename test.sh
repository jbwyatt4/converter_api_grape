#!/bin/bash

rails db:migrate # required for test db
rails db:test:prepare
bundle exec rspec
