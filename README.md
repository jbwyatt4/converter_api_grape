# README

This is a simple grape reference doc based on this [video](https://www.youtube.com/watch?v=fgn12-Yc-Sg) with rspec tests added.

To see the demo:

`./run.sh`

or

`bundle install && rails s`

Copy and paste into URL bar.

`http://localhost:3000/api/v1/converter/exchange?amount=4.5&to_currency=NTD`

To see places where code was added to config files search for:

`#[added] or #[added`

Some files of note:

1. app/api/converter/currency.rb
2. spec/requests/api/v1/converter_spec.rb
