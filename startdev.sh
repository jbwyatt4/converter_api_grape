#!/bin/bash

# Opens a new terminal tab
# Tested with gnome-terminal
WID=$(xprop -root | grep "_NET_ACTIVE_WINDOW(WINDOW)"| awk '{print $5}')
xdotool windowfocus $WID
xdotool key ctrl+t #ctrl+shift+t
wmctrl -i -a $WID
#  Opens a new browser window # Broken, try this in future? https://unix.stackexchange.com/questions/163793/start-a-background-process-from-a-script-and-manage-it-when-the-script-ends
xdg-open http://localhost:3000 &

# Install Local Gems
bundle install --without production
# Migrate DB
rails db:migrate
rails s
