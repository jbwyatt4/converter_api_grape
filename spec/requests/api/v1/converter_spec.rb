require 'rails_helper'

RSpec.describe "Converter", :type => :request do

  def converter_path
    "/api/v1/converter/exchange"
  end

  def is_json?(response)
    expect(response.content_type).to eq("application/json")
  end

  context 'Check if arbitary_default-to-NTD currency conversion works' do
    it 'converts 4.5 default currency to 135 NTD' do
      get converter_path + "?amount=4.5&to_currency=NTD"
      expect(is_json?(response))
      # print response.body.inspect
      json = JSON.parse(response.body)
      expect(json["status"]).to eq("success")
      expect(json["amount"]).to eq(135.0)
      expect(json["currency"]).to eq("NTD")
      expect(json["error_code"]).to eq(nil)
    end

    it 'returns \"failed\" + error message with the misspelled/nonexistant currency NTA' do
      get converter_path + "?amount=4.5&to_currency=NTA"
      expect(is_json?(response))
      # print response.body.inspect
      json = JSON.parse(response.body)
      expect(json["status"]).to eq("failed")
      expect(json["message"]).to eq("no conversion found for currency 'NTA'")
      expect(json["error_code"]).to eq(123)
    end
  end
end
