# #[added]
module Converter
  class Currency < Grape::API # this is mounted in routes.rb
    version 'v1', using: :path # declare a version using path
    format :json

    rescue_from :all do |e| # grape catches exceptions and reports/prints them as error messages in a desired format
      response_hash = {
        status: "failed",
        message: e.message,
        error_code: 123 #TODO: fix hard coded error num
      }
      error!(response_hash)
    end

    # how to control how errors are printed by grape
    # having issues with error_formatter with grape 1.2.5, used a block with rescue_from to solve
    # error_formatter :json, ->(message, backtrace, options, env) {
    #   {
    #     status: 'failed',
    #     message: message,
    #     error_code: 123 #TODO: fix hard coded error num
    #   }
    # }

    helpers do # grape's way to setup helper functions
      def get_exchange_rate(currency)
        case currency
        when 'NTD' #TODO: fix hard coded currency symbol
          30
        else
          raise StandardError.new "no conversion found for currency '#{currency}'"
        end
      end
    end

    resource :converter do
      params do # how to mandate params (handles conversions for you)
        # if required param not passed will return error with missing param
        requires :amount, type: Float
        requires :to_currency, type: String
      end
      get :exchange do
        converted_amount = params[:amount] *
          get_exchange_rate(params[:to_currency])
        {
          status: 'success',
          amount: converted_amount,
          currency: params[:to_currency],
          error_code: nil
        }
      end
    end
  end
end
